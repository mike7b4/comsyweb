pub const BODY: &'static str = r#"
<html>
  <head>
   <title>Comsy WEB!</title>
    <style>
    body {background-color: powderblue;}
    #send_to { width: 400px; }
    div#content {
      height: 800px;
      background-color: black;
      color: green; 
      overflow:scroll;
      margin: 3px 3px 3px 3px;
      padding: 3px 3px 3px 3px;
    }
    </style>
    </head>
    <script>
      var myVar = setInterval(function(){ refresh(); return true; }, 200);
      function submit(e) {
        alert(e)
      }
      function refresh() {
        const xhttp = new XMLHttpRequest();
        xhttp.onload = function() {
          if (this.responseText == "") {
            return;
          }
          var content = document.getElementById("content");
          content.innerHTML = content.innerHTML + this.responseText;
          content.scrollTop = content.scrollHeight;
        }
        xhttp.open("GET", "output", true);
        xhttp.send();
      }
      function setup_event() {
        const node = document.getElementById("send_to");
        node.addEventListener("keyup", function(event) {
            if (event.key === "Enter") {
                const xhttp = new XMLHttpRequest();
                xhttp.open("GET", "send/"+this.value, true);
                xhttp.send();
                this.value = ""
            }
        });
      }
    </script>
    <body onload="setup_event()">
    <h1>Comsy WEB</h1>
    <div id="content"></div>
    <label>Send:</label> <input type="text" name="send" id="send_to" ></input>
    </body>
</html>
"#;
