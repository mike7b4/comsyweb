mod comport_codec;
mod index;
use clap::Parser;
use comport_codec::LineCodec;

use futures::stream::StreamExt;
use futures::SinkExt;
use tokio_serial::SerialPortBuilderExt;
use tokio_util::codec::Decoder;

use std::sync::{Arc, Mutex};
use std::time::Duration;
use tokio::time::sleep;
use warp::Filter;
#[derive(Parser)]
#[clap(version, about)]
struct Args {
    #[clap(default_value = "/dev/ttyUSB0")]
    comport: std::path::PathBuf,
    #[clap(long, short, default_value = "3030")]
    listen_port: u16,
}

#[tokio::main]
async fn main() {
    let args = Args::parse();

    // data sent from com
    let web_rx = Arc::new(Mutex::new(Vec::new()));
    // passed to comport thread
    let web_tx = web_rx.clone();
    let index = warp::path::end().map(|| warp::reply::html(index::BODY));
    let out = warp::path!("output").map(move || {
        if let Ok(mut data) = web_rx.lock() {
            if data.is_empty() {
                format!("")
            } else {
                let mut lines = String::new();
                for d in data.drain(..) {
                    lines += &format!("{}", d);
                }
                lines
            }
        } else {
            format!("Mutex lock bailed")
        }
    });

    let com_rx = Arc::new(Mutex::new(Vec::new()));
    let com_tx = com_rx.clone();
    let com_rx2 = com_rx.clone();
    let send = warp::path!("send" / String).map(move |send: String| {
        if let Ok(mut data) = com_rx.lock() {
            if send == "%3" {
                data.push(String::from("\x03"));
            } else {
                data.push(format!("{}\r\n", send.replace("%20", " ")));
            }
        }
        format!("")
    });
    let com_rx3 = com_rx2.clone();
    let sigint = warp::path!("control" / "c").map(move || {
        if let Ok(mut data) = com_rx2.clone().lock() {
            data.push(String::from("\x03"));
        }
        format!("")
    });
    let sigd = warp::path!("control" / "d").map(move || {
        if let Ok(mut data) = com_rx3.clone().lock() {
            data.push(String::from("\x04"));
        }
        format!("")
    });

    let routes = warp::get().and(index).or(out).or(send).or(sigint).or(sigd);

    let comport = args.comport.clone();
    let comport = std::path::PathBuf::from(comport.as_path());
    let mut port = tokio_serial::new(comport.as_path().to_str().unwrap(), 115200)
        .open_native_async()
        .unwrap();
    port.set_exclusive(false).unwrap_or_else(|e| {
        log::warn!("Unable to set serial port exclusive to false cause: {}", e)
    });
    let (mut tx, mut rx) = LineCodec.framed(port).split();
    tokio::spawn(async move {
        while let Some(line_result) = rx.next().await {
            let line = line_result.expect("Failed to read line");
            let line = format!(
                "{}<br />",
                ansi_to_html::convert_escaped(&line).unwrap_or(line)
            );
            let mut data = web_tx.lock().unwrap();
            data.push(line);
        }
    });
    tokio::spawn(async move {
        loop {
            let s = if let Ok(mut data) = com_tx.lock() {
                if !data.is_empty() {
                    Some(data.remove(0))
                } else {
                    None
                }
            } else {
                None
            };
            if let Some(s) = s {
                tx.send(String::from(s)).await.ok();
            }
            sleep(Duration::from_millis(100)).await;
        }
    });

    log::info!("Listen port: {}", args.listen_port);
    warp::serve(routes)
        .run(([127, 0, 0, 1], args.listen_port))
        .await;
}
